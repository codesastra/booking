<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Busdetail extends Model
{
    public function Place()
    {
        return $this->belongsTo('App\Place');
    }

    public function Seatdetail()
    {
        return $this->belongsTo('App\Seatdetail');
    }
}
