<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
             'name' => 'Bishal',
             'email' => 'bsal@gmail.com',      
             'phoneno'=>'9845465',
            ]);
    }
}
