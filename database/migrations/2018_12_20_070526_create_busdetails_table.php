<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('busdetails', function (Blueprint $table) {
            $table->increments('bus_id');
            $table->string('busno');
            $table->unsignedInteger('type')->references('seat_type')->on('seatdetails')->onDelete('cascade');
            $table->integer('price');
            $table->unsignedInteger('location1')->references('place_id')->on('places')->onDelete('cascade');
            $table->unsignedInteger('location2')->references('place_id')->on('places')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('busdetails');
    }
}
