<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seatdetails', function (Blueprint $table) {
            $table->increments('seat_type');
            $table->integer('cabinset');
            $table->integer('aseat');
            $table->integer('bseat');
            $table->integer('lastseat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seatdetails');
    }
}
